FROM endeveit/docker-jq AS deps

# https://stackoverflow.com/a/58487433

COPY package.json /tmp
RUN jq '{ dependencies, devDependencies }' < /tmp/package.json > /tmp/deps.json

FROM node:12.18.2-alpine
ENV NODE_ENV production

WORKDIR /app

COPY --from=deps /tmp/deps.json ./package.json
COPY yarn.lock ./

RUN yarn

COPY . ./

EXPOSE 3000
