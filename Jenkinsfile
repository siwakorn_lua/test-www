pipeline {
  agent {
    dockerfile {
      filename 'builder.Dockerfile'
      args """${sh(returnStdout: true, script: 'echo --group-add ${AGENT_DOCKER_GID} -v ${AGENT_DOCKER_SOCK_PATH}')}"""
    }
  }
  environment {
    AWS_ACCESS_KEY_ID = credentials('aws-ecr-access-id')
    AWS_SECRET_ACCESS_KEY = credentials('aws-ecr-access-secret')
    IMAGE_URI = '729735958978.dkr.ecr.ap-southeast-1.amazonaws.com/test-www'
    HASH = """${sh(returnStdout: true, script: 'echo ${GIT_COMMIT} | cut -c 1-7')}"""
    VERSION = """${sh(returnStdout: true, script: 'node -p "require(\'./package.json\').version"')}"""
  }
  stages {
    stage('Pre-Build') {
      steps {
        sh 'aws --version'
        sh 'node --version'
        sh 'docker --version'
        sh 'export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}'
        sh 'export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}'
        sh 'aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin ${IMAGE_URI}'
      }
    }
    stage('Build') {
      steps {
        sh 'echo Build started on `date`'
        sh 'echo Building the app...'
        sh 'yarn --frozen-lockfile --prod=false'
        sh 'APP_ENV=${APP_ENV} yarn build'
        sh 'if [ ! -z "${WITH_STORYBOOK}" ] ; then yarn storybook:build ; fi'
        sh 'echo Building the image...'
        sh 'docker build -t ${IMAGE_URI}:${APP_ENV}-${HASH} -t ${IMAGE_URI}:${APP_ENV}-${VERSION} -t ${IMAGE_URI}:${APP_ENV}-latest .'
      }
    }
    stage('Post-Build') {
      steps {
        sh 'echo Build completed on `date`'
        sh 'echo Pushing the image...'
      }
    }
  }
}