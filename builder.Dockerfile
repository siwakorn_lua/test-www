FROM node:12.18.2-alpine

RUN apk update && apk add \
	ca-certificates \
	groff \
	less \
	python \
	py-pip \
	docker-cli \
	&& rm -rf /var/cache/apk/* \
  && pip install pip --upgrade \
  && pip install awscli